// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Starburst"
{
	Properties
	{
		_BlobPosition("BlobPosition", Vector) = (0,0,0,0)
		_MainTex("MainTex", 2D) = "white" {}
		_BlobSize("BlobSize", Float) = 5
		_StartRadius("StartRadius", Float) = 0
		_EndRadius("EndRadius", Float) = 5
		_VValue("VValue", Range( 0 , 1)) = 0
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 5.0
		#pragma only_renderers d3d9 d3d11 d3d11_9x 
		#pragma surface surf Standard alpha:fade keepalpha noshadow exclude_path:deferred noambient novertexlights nolightmap  nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float3 worldPos;
		};

		uniform float3 _BlobPosition;
		uniform float _StartRadius;
		uniform float _EndRadius;
		uniform sampler2D _MainTex;
		uniform float _BlobSize;
		uniform float _VValue;


		float2 UVMapper10( float Distance , float Size , float VValue )
		{
			float lerpOutput = Distance/Size;
			return float2(lerpOutput, VValue);
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float temp_output_5_0 = distance( ase_worldPos , _BlobPosition );
			float Distance10 = temp_output_5_0;
			float Size10 = _BlobSize;
			float VValue10 = _VValue;
			float2 localUVMapper10 = UVMapper10( Distance10 , Size10 , VValue10 );
			float4 tex2DNode8 = tex2D( _MainTex, localUVMapper10 );
			float4 _Color1 = float4(0,0,0,0);
			o.Emission = (( temp_output_5_0 >= _StartRadius && temp_output_5_0 <= _EndRadius ) ? tex2DNode8 :  _Color1 ).rgb;
			o.Alpha = (( temp_output_5_0 >= _StartRadius && temp_output_5_0 <= _EndRadius ) ? tex2DNode8.a :  _Color1.a );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15401
-1637;29;1630;1004;1685.782;368.3586;1.3;True;False
Node;AmplifyShaderEditor.Vector3Node;2;-1213.601,261.4998;Float;False;Property;_BlobPosition;BlobPosition;0;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;3;-1228.404,78.06871;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DistanceOpNode;5;-958.0018,187.2685;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-1193.083,-229.2587;Float;False;Property;_VValue;VValue;5;0;Create;True;0;0;False;0;0;0.3866256;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-1184.381,433.0418;Float;False;Property;_BlobSize;BlobSize;2;0;Create;True;0;0;False;0;5;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;10;-855.9802,-181.8573;Float;False;float lerpOutput = Distance/Size@$return float2(lerpOutput, VValue)@;2;False;3;True;Distance;FLOAT;0;In;;True;Size;FLOAT;5;In;;True;VValue;FLOAT;0;In;;UV Mapper;True;False;3;0;FLOAT;0;False;1;FLOAT;5;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;8;-638.8806,-211.0585;Float;True;Property;_MainTex;MainTex;1;0;Create;True;0;0;False;0;fbef07133c3dc224992a636fe385d23f;62cd2d9756ded224b958a8db91333abf;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;12;-1205.181,518.8414;Float;False;Property;_StartRadius;StartRadius;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;13;-1196.081,612.4415;Float;False;Property;_EndRadius;EndRadius;4;0;Create;True;0;0;False;0;5;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;7;-1246.255,-121.5986;Float;False;Constant;_Color1;Color 1;0;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCCompareWithRange;18;-224.5823,325.8419;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCCompareWithRange;15;-228.8813,135.3415;Float;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;32.47861,111.6298;Float;False;True;7;Float;ASEMaterialInspector;0;0;Standard;Starburst;False;False;False;False;True;True;True;True;True;True;True;True;False;False;True;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;False;0;False;Transparent;;Transparent;ForwardOnly;True;True;False;False;False;False;True;False;False;False;False;False;False;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;-1;False;-1;-1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;5;0;3;0
WireConnection;5;1;2;0
WireConnection;10;0;5;0
WireConnection;10;1;11;0
WireConnection;10;2;19;0
WireConnection;8;1;10;0
WireConnection;18;0;5;0
WireConnection;18;1;12;0
WireConnection;18;2;13;0
WireConnection;18;3;8;4
WireConnection;18;4;7;4
WireConnection;15;0;5;0
WireConnection;15;1;12;0
WireConnection;15;2;13;0
WireConnection;15;3;8;0
WireConnection;15;4;7;0
WireConnection;0;2;15;0
WireConnection;0;9;18;0
ASEEND*/
//CHKSM=122DD96719874572E43F7E6A4D46F8136ABED7D9
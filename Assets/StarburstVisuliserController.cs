﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct StarburstEffect
{
	public Vector2 vValueRange;
	public Vector2 minRadiusRange;
	public Vector2 maxRadiusRange;
	public Vector2 blobSizeRange;
	public float duration;
	public int textureIndex;
}

[Serializable]
public struct StarburstEffectsStack
{
	public string name;
	public bool loop;
	public List<StarburstEffect> stackOfEffects;
}

[Serializable]
public struct ListOfStarburstEffects
{
	public int currentEffectIndex;
	public List<StarburstEffectsStack> listOfEffects;
}

public class StarburstVisuliserController : MonoBehaviour
{

	public Material starburstMaterial;
	[HideInInspector]
	public Material defaultSpatialMeshMaterial;

	private bool _spatialMappingManagerDrawsMeshes;

	private Vector3 _starburstPosition;

	public float defaultBlobSize = 5f;

	[SerializeField] public ListOfStarburstEffects starburstEffects;
	private int _currentEffectIndex;
	private bool _isPlayingEffect = false;
	private bool _shouldLoop = false;
	private int _rippleEffectCurrentIndex = 0;
	
	private float timePast = 0;

	[HideInInspector]
	public float lastSetBlobRadius = 0;

	public List<Texture2D> listOfTextures;
	private int _lastTextureIndex;
	public Texture2D multiPatternTexture;
	public Texture2D rippleTexture;
	public bool debugMode = false;
	
	// Use this for initialization
//	void Start ()
//	{
//
//		if (debugMode)
//		{
//			TurnOnShader();
//		}
//	}

	public void SetStarburstPosition(Vector3 newPosition)
	{
		_starburstPosition = newPosition;
		starburstMaterial.SetVector("_BlobPosition", _starburstPosition);
	}


	public void PlayAnimation(Vector2 vRange, Vector2 minRadius, Vector2 maxRadius, Vector3 blobSizeRange, float duration = 0.3f, Action finishedCallback = null)
	{
		Action UpdateShader = () =>
		{
			starburstMaterial.SetFloat("_VValue", Mathf.Lerp(vRange.x, vRange.y, timePast));
			starburstMaterial.SetFloat("_StartRadius", Mathf.Lerp(minRadius.x, minRadius.y, timePast));
			starburstMaterial.SetFloat("_EndRadius", Mathf.Lerp(maxRadius.x, maxRadius.y, timePast));
			starburstMaterial.SetFloat("_BlobSize", Mathf.Lerp(blobSizeRange.x, blobSizeRange.y, timePast));

		};

		StartCoroutine(PlayStarburstAnimation(UpdateShader, duration, finishedCallback));
		

	}
	
	public void HideShader()
	{
		starburstMaterial.SetFloat("_BlobSize", 0f);
	}

	public void ShowShader(float previousBlobSize = 0)
	{
		starburstMaterial.SetFloat("_BlobSize", defaultBlobSize);
	}

	public void UpdateBlobRadius(float radius)
	{
		lastSetBlobRadius = radius;
		starburstMaterial.SetFloat("_BlobSize", radius);
	}

	private IEnumerator PlayStarburstAnimation(Action UpdateFunction, float duration, Action finishedCallback = null)
	{
		var timeMultiplier = 1 / duration;
		timePast = 0;
		
		do
		{
			UpdateFunction();

			timePast += Time.deltaTime * timeMultiplier;
			yield return null;
		} while (timePast < 1);

		UpdateFunction();

		lastSetBlobRadius = starburstMaterial.GetFloat("_BlobSize");
		
		finishedCallback?.Invoke();
		
		yield break;
	}

//	public void TurnOnShader()
//	{
//		SpatialMappingManager.Instance.DrawVisualMeshes = true;
//		
//		SpatialMappingManager.Instance.SurfaceMaterial = starburstMaterial;
//		
//	}
//
//	public void TurnOffShader()
//	{
//		SpatialMappingManager.Instance.SurfaceMaterial = defaultSpatialMeshMaterial;
//		SpatialMappingManager.Instance.DrawVisualMeshes = _spatialMappingManagerDrawsMeshes;
//	}

	
	public void TestPlayEffect()
	{
		if (_isPlayingEffect)
		{
			StopEffect();
		}
		PlayStarburstEffect(starburstEffects.currentEffectIndex);
		_isPlayingEffect = true;
		
	}

	public void StopEffect()
	{
		StopAllCoroutines();
		_isPlayingEffect = false;
	}

	public void PlayEffect()
	{
		PlayStarburstEffect(starburstEffects.currentEffectIndex);
		_isPlayingEffect = true;
		
	}

	public void PlayNewEffect(int index)
	{
		StopEffect();
		starburstEffects.currentEffectIndex = index;
		PlayEffect();
	}
	
	public void PlayStarburstEffect(int stackIndex)
	{
		var currentEffectStack = starburstEffects.listOfEffects[stackIndex];
		var numOfEffectsInStack = currentEffectStack.stackOfEffects.Count;
		_currentEffectIndex = _currentEffectIndex % numOfEffectsInStack;
		_shouldLoop = currentEffectStack.loop;
		
		var currentEffect = currentEffectStack.stackOfEffects[_currentEffectIndex];
		if (currentEffect.textureIndex != _lastTextureIndex)
			SwitchTexture(currentEffect.textureIndex);
		
		PlayAnimation(currentEffect.vValueRange, currentEffect.minRadiusRange, currentEffect.maxRadiusRange,
			currentEffect.blobSizeRange, currentEffect.duration, () =>
			{
				if (_shouldLoop)
				{
					PlayEffect();
				}
				else
				{
					StopEffect();
				}
			});
		
		_currentEffectIndex++;
	}

	public void SwitchTexture(int textureIndex)
	{
		starburstMaterial.mainTexture = listOfTextures[textureIndex];
		_lastTextureIndex = textureIndex;
	}
}

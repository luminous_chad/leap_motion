﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Leap.Unity;
using TMPro;
using UnityEngine;

public class TeleportController : MonoBehaviour
{

	public GameObject playerRig;
	private Transform _playerHead;
	public GameObject hitPointGameObject;
	private bool _isTeleporting;
	public float maxDistance = 3f;
	public LayerMask teleportLayers;
	public TextMeshPro teleportButtonText;

	public bool useTimer = false;
	public float gazeTimer = 1f;
	public float gazeRestDisplacement = 0.2f;
	private float _gazeTime = 0f;
	private Vector3 _previousGazePosition;

	public StarburstVisuliserController gazeVisuliser;
	private int _currentEffectIndex = -1;
	public GameObject fakePlane;

	private void Start()
	{
		_playerHead = Camera.main.transform;
		hitPointGameObject.SetActive(false);
		fakePlane.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		if (_isTeleporting)
		{
			var beamOrigin = _playerHead.position;
			var beamDirection = _playerHead.forward;

			RaycastHit hitInfo;
			if (Physics.Raycast(beamOrigin, beamDirection, out hitInfo, maxDistance, teleportLayers))
			{
				if (!hitPointGameObject.activeSelf)
				{
					hitPointGameObject.SetActive(true);
				}
				hitPointGameObject.transform.position = hitInfo.point;
				gazeVisuliser.SetStarburstPosition(hitInfo.point);
			}
			else
			{
				hitPointGameObject.SetActive(false);
			}

			if ((useTimer && _gazeTime > gazeTimer))
			{
				Teleport();
			}
		}
	}

	private IEnumerator MeasureGazeDisplacementAndtime()
	{
		while (_isTeleporting)
		{
			if (Mathf.Abs((_previousGazePosition - hitPointGameObject.transform.position).magnitude) > gazeRestDisplacement)
			{
				_gazeTime = 0f;
				if (_currentEffectIndex != 0)
				{
					gazeVisuliser.PlayNewEffect(0);
					_currentEffectIndex = 0;
				}
			}
			else
			{
				_gazeTime += gazeTimer * 0.1f;
				if (_currentEffectIndex != 1)
				{
					var effect = gazeVisuliser.starburstEffects.listOfEffects[1].stackOfEffects[0];
					effect.duration = gazeTimer;
					gazeVisuliser.starburstEffects.listOfEffects[1].stackOfEffects[0] = effect;
					gazeVisuliser.PlayNewEffect(1);
					_currentEffectIndex = 1;
				}
			}

			_previousGazePosition = hitPointGameObject.transform.position;
			yield return new WaitForSeconds(gazeTimer * 0.1f);
		}
	}

	public void EnableTeleport()
	{
		_isTeleporting = true;
		teleportButtonText.text = "Gaze at location...";
		if (useTimer)
		{
			fakePlane.SetActive(true);
			StartCoroutine(MeasureGazeDisplacementAndtime());
		}
		
	}

	public void DisableTeleport()
	{
		teleportButtonText.text = "Teleport";
		_isTeleporting = false;
		hitPointGameObject.SetActive(false);
		if (useTimer)
		{
			fakePlane.SetActive(false);
		}
	}

	public void Teleport()
	{
		if (_isTeleporting)
		{
			if ((useTimer && _gazeTime > gazeTimer) || !useTimer)
			{
				playerRig.transform.position = hitPointGameObject.transform.position;
				_gazeTime = 0;
			}
		}
		DisableTeleport();
	}

	public void ToggleTeleport()
	{
		if (_isTeleporting)
		{
			DisableTeleport();
		}
		else
		{
			EnableTeleport();
		}
	}
	
	
}
